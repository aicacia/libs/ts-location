let IS_MOBILE = false;
let CALLED = false;

export function isMobile() {
  if (!CALLED) {
    CALLED = true;
    IS_MOBILE = window != null && "ontouchstart" in window;
  }
  return IS_MOBILE;
}

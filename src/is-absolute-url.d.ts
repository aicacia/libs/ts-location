declare module "is-absolute-url" {
  function isAbsoluteURL(url: string): boolean;
  export = isAbsoluteURL;
}

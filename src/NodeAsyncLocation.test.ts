import * as tape from "tape";
import { parse, UrlWithParsedQuery } from "url";
import { NodeAsyncLocation } from "./NodeAsyncLocation";

const URL = "https://example.org/";

tape("location setOnly setHandler setHtml5Mode", (assert: tape.Test) => {
  const location = new NodeAsyncLocation(parse(URL, true));

  location.init();

  location.setOnly("/pathname");
  assert.equals(location.get().pathname, "/pathname");

  location.setHandler((prevHandler) => (url: UrlWithParsedQuery) =>
    prevHandler(url)
  );

  location.remove();

  assert.end();
});

tape("location history", (assert: tape.Test) => {
  const location = new NodeAsyncLocation(parse(URL, true));

  location.init().then(() => {
    location.set("/pathname").then(() => {
      assert.equals(location.get().pathname, "/pathname");

      location.back();

      setTimeout(() => {
        assert.equals(location.get().pathname, "/");

        location.forward();

        setTimeout(() => {
          assert.equals(location.get().pathname, "/pathname");
          assert.equals(location.getHistory().length, 2);
          assert.end();
        }, 10);
      }, 10);
    });
  });
});

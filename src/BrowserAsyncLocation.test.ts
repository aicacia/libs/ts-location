/// <reference types="./add-dom-event-listener" />
/// <reference types="./is-absolute-url" />
/// <reference types="./same-origin" />

// @ts-ignore
import { JSDOM } from "jsdom";
import * as tape from "tape";
import { UrlWithParsedQuery } from "url";
import { BrowserAsyncLocation } from "./BrowserAsyncLocation";

const URL = "https://example.org/";

function createJSDOM() {
  const jsdom = new JSDOM(`<!DOCTYPE html><body></body>`, {
    url: URL,
    referrer: URL,
  });

  (global as any).window = jsdom.window;
  (global as any).document = jsdom.document;

  return jsdom;
}

tape("location setOnly setHandler setHtml5Mode", (assert: tape.Test) => {
  createJSDOM();

  const location = new BrowserAsyncLocation();

  location.init();

  location.setOnly("/pathname");
  assert.equals(location.get().pathname, "/pathname");

  location.setHandler((prevHandler) => (url: UrlWithParsedQuery) =>
    prevHandler(url)
  );
  location.setHtml5Mode(false);

  location.remove();

  assert.end();
});

tape("location history", (assert: tape.Test) => {
  const jsdom = createJSDOM();

  const location = new BrowserAsyncLocation(false);

  location.init().then(() => {
    jsdom.window.location.hash = "/pathname";

    setTimeout(() => {
      assert.equals(location.get().pathname, "/pathname");

      location.back();

      setTimeout(() => {
        assert.equals(location.get().pathname, "/");

        location.forward();

        setTimeout(() => {
          assert.equals(location.get().pathname, "/pathname");
          assert.equals(location.getHistory().length, 2);
          assert.end();
        }, 10);
      }, 10);
    });
  });
});

tape("location can set the pathname", (assert: tape.Test) => {
  createJSDOM();

  const location = new BrowserAsyncLocation();

  location.set("/pathname/a/b/c").then(() => {
    assert.equals(
      location.get().pathname,
      "/pathname/a/b/c",
      "location.set('/pathname/a/b/c') sets the path to '/pathname/a/b/c'"
    );
    location.remove();
    assert.end();
  });
});

tape(
  "location set when returned an error does not change the pathname",
  (assert: tape.Test) => {
    createJSDOM();

    const handler = (url: UrlWithParsedQuery) =>
      Promise.reject(new Error(url.pathname as string));

    const location = new BrowserAsyncLocation(false, handler);

    location
      .set("/pathname")
      .catch((e) =>
        assert.equals(
          !!e,
          true,
          "a returned rejected promise causes and error to exist."
        )
      )
      .then(() => {
        assert.equals(
          location.get().pathname,
          "/",
          "the path is not changed when a rejected promise is returned"
        );
        location.remove();
        assert.end();
      });
  }
);

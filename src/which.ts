export function which(event: MouseEvent): number {
  return event
    ? event.which === void 0 || event.which === null
      ? +event.button
      : +event.which
    : 0;
}

import { UrlWithParsedQuery, resolve, parse } from "url";
import { EventEmitter } from "events";
import { isString } from "util";
import { IHandler } from "./IHandler";
import { urlsEqual } from "./urlsEqual";
import { defaultHandler } from "./defaultHandler";
import { getUrlPathname } from "./getUrlPathname";

export abstract class AsyncLocation extends EventEmitter {
  private initted = false;
  protected handler: IHandler;
  protected url: UrlWithParsedQuery;
  protected history: UrlWithParsedQuery[];
  protected historyIndex = 0;

  constructor(url: UrlWithParsedQuery, handler: IHandler = defaultHandler) {
    super();

    this.handler = handler;
    this.url = url;
    this.history = [url];
  }

  abstract onRemove(): void;
  abstract onInit(): void;
  abstract replaceState(url: UrlWithParsedQuery): void;
  abstract setState(url: UrlWithParsedQuery): void;
  abstract backState(previousUrl: UrlWithParsedQuery): void;
  abstract forwardState(nextUrl: UrlWithParsedQuery): void;

  init() {
    if (!this.initted) {
      this.initted = true;
      this.onInit();
      this.emit("init");
    }
    return this.internalSetUrl(this.url, false, true);
  }
  remove() {
    if (this.initted) {
      this.emit("remove");
      this.initted = false;
      this.onRemove();
    }
  }

  updateHistory(url: UrlWithParsedQuery): void {
    if (this.historyIndex < this.history.length - 1) {
      this.history.splice(this.historyIndex, 0, url);
    } else {
      this.history.push(url);
    }
    this.historyIndex += 1;
  }

  get(): UrlWithParsedQuery {
    return this.url;
  }
  getOrigin(): string {
    return `${this.url.protocol || "http:"}//${
      this.url.hostname || "localhost"
    }${this.url.port ? ":" + this.url.port : ""}`;
  }

  getHandler(): IHandler {
    return this.handler;
  }
  setHandler(setHandlerFn: (prevHandler: IHandler) => IHandler) {
    this.handler = setHandlerFn(this.handler);
    return this;
  }

  getHistory(): ReadonlyArray<UrlWithParsedQuery> {
    return this.history;
  }

  getRelativeUrl(
    pathOrUrl: UrlWithParsedQuery | string = ""
  ): UrlWithParsedQuery {
    if (isString(pathOrUrl)) {
      return parse(resolve(this.getOrigin(), pathOrUrl), true);
    } else {
      return parse(resolve(this.getOrigin(), getUrlPathname(pathOrUrl)), true);
    }
  }

  set(
    pathOrUrl: UrlWithParsedQuery | string,
    force?: boolean
  ): Promise<UrlWithParsedQuery> {
    return this.internalSetUrl(this.getRelativeUrl(pathOrUrl), true, force);
  }
  setOnly(pathOrUrl: UrlWithParsedQuery | string) {
    this.url = this.getRelativeUrl(pathOrUrl);
    this.replaceState(this.url);
    this.emit("set-only", this.url);
    return this;
  }

  back() {
    if (this.history.length >= 1) {
      let index = this.historyIndex;

      if (index > 0) {
        index -= 1;
        this.historyIndex = index;
        this.backState(this.history[index]);
      }
    }
  }

  forward() {
    if (this.history.length >= 1) {
      let index = this.historyIndex;

      if (index < this.history.length - 1) {
        index += 1;
        this.historyIndex = index;
        this.forwardState(this.history[index]);
      }
    }
  }

  protected internalSetUrl(
    url: UrlWithParsedQuery,
    updateHistory = true,
    force = false
  ): Promise<UrlWithParsedQuery> {
    if (force || !urlsEqual(url, this.url)) {
      this.emit("set", url);

      return this.handler(url).then(
        () => {
          if (updateHistory) {
            this.updateHistory(url);
          }

          this.setState(url);
          this.url = url;

          this.emit("set-success", url);
          return url;
        },
        (url: UrlWithParsedQuery | Error | null) => {
          if (url instanceof Error) {
            this.emit("set-error", url);
            throw url;
          } else if (url && url.pathname) {
            return this.set(url);
          } else {
            return this.set(this.url);
          }
        }
      );
    } else {
      return Promise.resolve(this.url);
    }
  }
}

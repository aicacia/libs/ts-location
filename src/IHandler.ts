import { UrlWithParsedQuery } from "url";

export type IHandler = (
  url: UrlWithParsedQuery
) => Promise<UrlWithParsedQuery | null | undefined>;

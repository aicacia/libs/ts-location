import { UrlWithParsedQuery } from "url";
import { IHandler } from "./IHandler";

export const defaultHandler: IHandler = (url: UrlWithParsedQuery) =>
  Promise.resolve(url);

declare module "same-origin" {
  function sameOrigin(a: string, b: string): boolean;
  export = sameOrigin;
}

import { UrlWithParsedQuery } from "url";
import { AsyncLocation } from "./AsyncLocation";

export class NodeAsyncLocation extends AsyncLocation {
  onRemove() {}

  onInit() {}

  replaceState(url: UrlWithParsedQuery): void {
    this.url = url;
  }
  setState(url: UrlWithParsedQuery): void {
    this.url = url;
  }
  backState(previousUrl: UrlWithParsedQuery): void {
    this.url = previousUrl;
  }
  forwardState(nextUrl: UrlWithParsedQuery): void {
    this.url = nextUrl;
  }
}

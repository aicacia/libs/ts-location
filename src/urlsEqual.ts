import { UrlWithParsedQuery } from "url";

export const urlsEqual = (
  a: UrlWithParsedQuery,
  b: UrlWithParsedQuery
): boolean =>
  a.pathname === b.pathname && a.search === b.search && a.hash === b.hash;

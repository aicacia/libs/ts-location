import addEventListener = require("add-dom-event-listener");
import sameOrigin = require("same-origin");
import isAbsoluteUrl = require("is-absolute-url");
import { UrlWithParsedQuery, parse } from "url";
import { AsyncLocation } from "./AsyncLocation";
import { getUrlPathname } from "./getUrlPathname";
import { IHandler } from "./IHandler";
import { which } from "./which";
import { isMobile } from "./isMobile";
import { hasHtml5Mode } from "./hasHtml5Mode";

export class BrowserAsyncLocation extends AsyncLocation {
  private html5Mode = false;
  private settingHash = false;
  private removeOnClick?: { remove(): void };
  private removeOnPopState?: { remove(): void };
  private removeOnHashChange?: { remove(): void };

  constructor(html5Mode?: boolean, handler?: IHandler) {
    super(parse(window.location.href, true), handler);

    if (hasHtml5Mode() && html5Mode) {
      this.html5Mode = html5Mode;
    }
  }

  onInit() {
    this.removeOnClick = addEventListener(
      window,
      isMobile() ? "touchend" : "click",
      this.onClick
    );
    this.removeOnPopState = addEventListener(
      window,
      "popstate",
      this.onPopState
    );
    this.removeOnHashChange = addEventListener(
      window,
      "hashchange",
      this.onHashChange
    );
  }
  onRemove() {
    this.removeOnClick?.remove();
    this.removeOnPopState?.remove();
    this.removeOnHashChange?.remove();
  }

  getHtml5Mode() {
    return this.html5Mode;
  }
  setHtml5Mode(html5Mode = true) {
    if (hasHtml5Mode()) {
      this.html5Mode = html5Mode;
    }
    return this;
  }

  replaceState(url: UrlWithParsedQuery): void {
    if (this.html5Mode && hasHtml5Mode()) {
      window.history.replaceState(
        url,
        url.href || url.pathname || "",
        url.href || url.pathname || ""
      );
    } else {
      window.location.hash = getUrlPathname(url);
    }
  }
  setState(url: UrlWithParsedQuery): void {
    if (this.html5Mode && hasHtml5Mode()) {
      window.history.pushState(
        url,
        url.href || url.pathname || "",
        url.href || url.pathname || ""
      );
    } else {
      window.location.hash = getUrlPathname(url);
    }
  }
  backState(previousUrl: UrlWithParsedQuery): void {
    if (this.html5Mode && hasHtml5Mode()) {
      window.history.back();
    } else {
      this.settingHash = true;
      window.location.hash = getUrlPathname(previousUrl);
    }
  }
  forwardState(nextUrl: UrlWithParsedQuery): void {
    if (this.html5Mode && hasHtml5Mode()) {
      window.history.forward();
    } else {
      this.settingHash = true;
      window.location.hash = getUrlPathname(nextUrl);
    }
  }
  pushHistory(url: UrlWithParsedQuery): void {
    if (this.historyIndex < this.history.length - 1) {
      this.history.splice(this.historyIndex, 0, url);
    } else {
      this.history.push(url);
    }
    this.historyIndex += 1;
  }

  private onHashChange = (event: Event): boolean | undefined => {
    if (this.html5Mode && hasHtml5Mode()) {
      event.preventDefault();
      return false;
    } else {
      let updateHistory = true;

      if (this.settingHash) {
        this.settingHash = false;
        updateHistory = false;
      }
      this.internalSetUrl(
        this.getRelativeUrl((window.location.hash || "#/").slice(1) || ""),
        updateHistory
      );
    }
  };

  private onPopState = (event: any) => {
    event.preventDefault();

    if (event.nativeEvent && event.nativeEvent.state) {
      this.internalSetUrl(parse(event.nativeEvent.state.href, true), true);
    }

    return undefined;
  };

  private onClick = (event: MouseEvent): boolean | undefined => {
    // ignore if not normal left click
    if (
      which(event) !== 1 ||
      event.metaKey ||
      event.ctrlKey ||
      event.shiftKey ||
      event.defaultPrevented
    ) {
      return;
    }

    // find a a tag in the target's parents
    let el = event.target as Element;
    while (el && el.nodeName !== "A") {
      el = el.parentNode as Element;
    }

    // ignore if not a tag or is download or external
    if (
      !el ||
      "A" !== el.nodeName ||
      el.getAttribute("download") ||
      el.getAttribute("rel") === "external"
    ) {
      return;
    }

    const href = (el as HTMLAnchorElement).href;
    let link: string = el.getAttribute("href") || href || "";

    if (!link || (el as HTMLAnchorElement).target) {
      return;
    }

    link = link[0] === "#" ? link.slice(1) : link;

    if (link && (link.indexOf("mailto:") > -1 || link.indexOf("tel:") > -1)) {
      return;
    }

    if (
      (href && !sameOrigin(this.getOrigin(), href)) ||
      (isAbsoluteUrl(link) && !sameOrigin(this.getOrigin(), link))
    ) {
      return;
    }

    event.preventDefault();

    this.set(this.getRelativeUrl(link));

    return false;
  };
}

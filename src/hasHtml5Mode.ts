let HAS_HTML5_MODE = false;
let CALLED = false;

export function hasHtml5Mode() {
  if (!CALLED) {
    CALLED = true;
    HAS_HTML5_MODE =
      window != null &&
      window.history != null &&
      window.history.pushState != null;
  }
  return HAS_HTML5_MODE;
}

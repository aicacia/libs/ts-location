import { UrlWithParsedQuery } from "url";

export const getUrlPathname = (url: UrlWithParsedQuery): string =>
  url.pathname + (url.search || "") + (url.hash || "");
